import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs'; 
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  
  
  signup(email:string, password:string)
  {
    return this.fireBaseAuth
    .auth
    .createUserWithEmailAndPassword(email,password);
  }

  updateProfile(user,name:string){
    // השם שיגיע מהסוגריים למעלה ייכנס לתוך דיספלייניימ שזהו שם שמור
    user.updateProfile({displayName:name, photoURL:''}) // אפדייטפרופייל זו פונקציה של יוזר שמעדכנת את הפרופיל של יוזר
  }

  login(email:string, password:string){
  return this.
  fireBaseAuth.
  auth.
  signInWithEmailAndPassword(email, password)
  }

  logout(){
    return this.
            fireBaseAuth.
            auth.signOut()
  }

    // הוספת end point to user
  // מייצרת יוזר בכל פעם שיוזר חדש מבצע הרשמה
  addUser(user, name : string){
    // לכל יוזר יש יוזר איי די, בעזרתו נבנה את נקודת הקצה. אנו נקבל את יוזר איי די מהאובייקט של היוזר
    let uid = user.uid;
    let ref = this.db.database.ref('/'); // נקודת קצה הראשית של הדטהבייס 
    ref.child('users').child(uid).push({'name': name}); // יצירת אנדפוינט חדש כי הוא לא קיים בגלל שמדובר בתהליך ההרשמה
  // שמירת מידע לפיירבייס נעשית באמצעות הפונקציה פוש
  }

  user:Observable<firebase.User>; // בשביל לברך לשלום את המשתמש
// אובסרבייבל , אני יכול לבצע רישום ולחכות לקבל מידע

  constructor(private fireBaseAuth:AngularFireAuth, //מסוג אנגולרפייראוט'
  private db:AngularFireDatabase) // injection
  {
    this.user = fireBaseAuth.authState; // מצב האוטנטיקיישן יודע להגיד דברים על היוזר
//האובסרבייל תמיד מאזין לאוט' סטטיט
  }
}