import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from '../auth.service';


@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {


  toRegister(){
    this.router.navigate(['/register'])
  }


  toCodes(){
    this.router.navigate(['/codes'])
  }

  userTodos(){
    this.router.navigate(['/usertodos'])
  }

// err , value הן לא מילים שמורות
  logout(){
    this.authService.logout().
    then(value => {
      this.router.navigate(['/login'])
    }).catch(err =>{console.log(err)})
  }

  constructor(private router:Router,  public authService:AuthService) { }

  ngOnInit() {
  }

}
