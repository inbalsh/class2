import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service'; // הוספת אימפורט
import {Router} from "@angular/router";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  // הוספת משתנים והסוג שלהם
  email: string;
  password: string;
  name: string;
  error = '';  // הוספת תכונה של המחלקה
  message ='' ;
  code = '';
  
  signup()
  {
  //  console.log("sign up clicked" + " " + this.email + " " + this.password + " " + this.name)
    this.authService.signup(this.email,this.password)   // promise
      .then(value => {
    //    console.log(value)
      // then משמע - ברגע שמתרחש אירוע אז.......
      // בסוגריים נשתמש ב arroe function
      // הוליו זה מה שפיירבייס מחזיר לנו
      // כדי לדעת ממה הויליו שהגיע מפיירבייס מורכב, נכתוב קונסול
      this.authService.updateProfile(value.user,this.name); // ליוזר שיצרת תוסיפי את השם הזה
      this.authService.addUser(value.user, this.name); //הוספת שם
      }).then(value=>{
        this.router.navigate(['/']);
        // במידה ויש שגיאה לא ילך לכל הט'נ שכתבנו קודם אלא יבוא ישר לקטצ'..ההבטחה לא התקיימה
       }).catch(err => { // המטרה ללכוד שגיאות ולדווח למשתמש
      // אם ההודעה היא תכונה של המחלקה, ניתן להעבירה לטמפלייט
      this.message = err.message;
      this.code = err.code;
      console.log(err);
  })
  }

  constructor(public authService:AuthService,  private router:Router) { }

  ngOnInit() {
  }

}
