import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה
import {AuthService} from '../auth.service'; // הוספת אימפורט
import { TodosService } from '../todos.service';

@Component({
  selector: 'todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {


  todos = [];
  /*
  todos = [
    {"text":"Study json", "id":1},
    {"text":"Do final project", "id":2},
    {"text":"Do test", "id":3},  
  ]
*/
  todoTextFromTodo = 'No text so far';

  text:string; // הוספת תכונה טקסט לטודו כדי לעבוד עם האנג'ימודל
  
  addTodo(){
    this.todosService.addTodo(this.text);
    this.text =''; // לרוקן את שדה הטקסט בטופס, לאפס, אחרי שהוא מוסיף את המשימה
  }
 
  showText($event){
    this.todoTextFromTodo = $event;
  }


constructor(private db:AngularFireDatabase,
         private authService:AuthService,
         private todosService:TodosService) // החיבור לעולם הפיירבייס
   {

    }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/todos').snapshotChanges().subscribe( // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
        todos =>{ // אינפוט טודוס מפיירבייס
          this.todos = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
          todos.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס מפיירבייס. זוהי לולאה שרצה על כל האובייקטים ברשימה בפיירבייס - קי 
            todo => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
              let y = todo.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
              y["$key"] = todo.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
              this.todos.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
              // לכל טודו יש שני שדות: קי ופיילוד (התוכן). ולכן ניתן לעשות טודו נקודה קי או נקודה פיילוד
            }
          )
        }
      )
    })
  } 
}
