import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה

@Component({
  selector: 'usertodos',
  templateUrl: './usertodos.component.html',
  styleUrls: ['./usertodos.component.css']
})
export class UsertodosComponent implements OnInit {

  user = 'jack';

  todoTextFromTodo = 'No text so far';
 
  showText($event){
    this.todoTextFromTodo = $event;
  }

  todos = [];

  constructor(private db:AngularFireDatabase) { }

 //פונקציה כדי להראות את המשימות של כל  משתמש שנזין את שמו ולא רק של ג'ק
 changeuser(){ 
  this.db.list('/users/' + this.user + '/todos').snapshotChanges().subscribe( // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
    todos =>{
      this.todos = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
      todos.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס. זוהי לולאה שרצה על כל האובייקטים ברשימה - קי 
        todo => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
          let y = todo.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
          y["$key"] = todo.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
          this.todos.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
        }

      )

    }
  )
  }

  ngOnInit() {
//    this.authService.user.subscribe(user => {
      this.db.list('/users/'+this.user+'/todos').snapshotChanges().subscribe( // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
        todos =>{ // אינפוט טודוס מפיירבייס
          this.todos = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
          todos.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס מפיירבייס. זוהי לולאה שרצה על כל האובייקטים ברשימה בפיירבייס - קי 
            todo => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
              let y = todo.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
              y["$key"] = todo.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
              this.todos.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
              // לכל טודו יש שני שדות: קי ופיילוד (התוכן). ולכן ניתן לעשות טודו נקודה קי או נקודה פיילוד
            }
          )
        }
      )
 //   })
  }

  }

//}
