import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { TodosService } from '../todos.service';


@Component({
  selector: 'todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  @Input() data:any; // הגדרת אינפוט עם תכונה דטה
  @Output() myButtonClicked = new EventEmitter<any>(); // הגדרת אאוטפוט מסוג איבנט-אמיטר

  text;
  //id;
  key; 
  showTheButton = false; // אחראי על הצגת כפתורים
  showEditField = false;
  tempText; // לשמירת הערך אם נלחץ על ביטול

  
  // הפונקציה סנד פולטת את האירוע אל האב ומוסיפה מידע לגבי הטקסט
  send(){
    console.log('event caught');
    this.myButtonClicked.emit(this.text);
  }

  showButton(){
    this.showTheButton = true; // אחראי על הצגת כפתורים
    console.log ("works");
  }
  hideButton(){
    this.showTheButton = false; // אחראי על הסתרת כפתורים
    console.log ("works");
  }

  delete(){
    this.todoService.delete(this.key)
    console.log ("works");
  }

  showEdit()
  {
    this.showEditField = true;
    this.tempText = this.text;
  }

  save(){
    this.todoService.update(this.key, this.text)
    this.showEditField = false;
  }
  
  cancel(){
    this.showEditField = false;
    this.text = this.tempText;
  }

constructor(private todoService:TodosService) { }

  ngOnInit() {
      // בעת יצירת אלמנט טודו חדש, הפונקציה הזו רצה
      this.text = this.data.text;
    //  this.id = this.data.id;
    this.key = this.data.$key; // מעדכנת את הפרמטר קי עם המשתנה שהוכנס

  }

}
