// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyAGy9HqVldWbxM3cMS1mGWcXHB5Drh3098",
  authDomain: "class2-48fd5.firebaseapp.com",
  databaseURL: "https://class2-48fd5.firebaseio.com",
  projectId: "class2-48fd5",
  storageBucket: "class2-48fd5.appspot.com",
  messagingSenderId: "664615744599"
  }
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
